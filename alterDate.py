

with open('flowmeter-aggregate.sql', 'r') as input_file, open('output.sql', 'w') as output_file:
    for line in input_file:
        if("01/2022" in line):
            line = line.replace("01/2022", "2022-01-01 00:00:00")
        if("02/2022" in line):
            line = line.replace("02/2022", "2022-02-01 00:00:00")
        if("03/2022" in line):
            line = line.replace("03/2022", "2022-03-01 00:00:00")
        if("04/2022" in line):
            line = line.replace("04/2022", "2022-04-01 00:00:00")
        if("05/2022" in line):
            line = line.replace("05/2022", "2022-05-01 00:00:00")
        if("06/2022" in line):
            line = line.replace("06/2022", "2022-06-01 00:00:00")
        if("07/2022" in line):
            line = line.replace("07/2022", "2022-07-01 00:00:00")
        if("08/2022" in line):
            line = line.replace("08/2022", "2022-08-01 00:00:00")
        if("09/2022" in line):
            line = line.replace("09/2022", "2022-09-01 00:00:00")
        if("10/2022" in line):
            line = line.replace("10/2022", "2022-10-01 00:00:00")
        if("11/2022" in line):
            line = line.replace("11/2022", "2022-11-01 00:00:00")
        if("12/2022" in line):
            line = line.replace("12/2022", "2022-12-01 00:00:00")
        output_file.write(line)