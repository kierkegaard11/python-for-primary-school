import pandas
import matplotlib.pyplot as plt
import numpy

#Za dataset iz knjige nije data dokumentacija,
#pa se ne moze znati sta predstavljaju kolone
#zaista...

#Kolone
"""
    Div: Division or league name (in this case, German Bundesliga).
    Date: Date of the match.
    HomeTeam: Name of the home team.
    AwayTeam: Name of the away team.
    FTHG: Full-time home team goals.
    FTAG: Full-time away team goals.
    FTR: Full-time result (H: Home team win, A: Away team win, D: Draw).
    HTHG: Half-time home team goals.
    HTAG: Half-time away team goals.
    HTR: Half-time result (H: Home team leading, A: Away team leading, D: Draw).
    HS: Home team shots.
    AS: Away team shots.
    HST: Home team shots on target.
    AST: Away team shots on target.
    HF: Home team fouls committed.
    AF: Away team fouls committed.
    HC: Home team corners.
    AC: Away team corners.
    HY: Home team yellow cards.
    AY: Away team yellow cards.
    HR: Home team red cards.
    AR: Away team red cards.
    B365H: Betting odds for home team win (from Bet365).
    B365D: Betting odds for draw (from Bet365).
    B365A: Betting odds for away team win (from Bet365).
    BWH: Betting odds for home team win (from Betandwin).
    BWD: Betting odds for draw (from Betandwin).
    BWA: Betting odds for away team win (from Betandwin).
    IWH: Betting odds for home team win (from Interwetten).
    IWD: Betting odds for draw (from Interwetten).
    IWA: Betting odds for away team win (from Interwetten).
    PSH: Betting odds for home team win (from Pinnacle Sports).
    PSD: Betting odds for draw (from Pinnacle Sports).
    PSA: Betting odds for away team win (from Pinnacle Sports).
    WHH: Betting odds for home team win (from William Hill).
    WHD: Betting odds for draw (from William Hill).
    WHA: Betting odds for away team win (from William Hill).
    VCH: Betting odds for home team win (from VC Bet).
    VCD: Betting odds for draw (from VC Bet).
    VCA: Betting odds for away team win (from VC Bet).
    Bb1X2: Number of bookmakers used to calculate match odds.
    BbMxH: Maximum betting odds for home team win across bookmakers.
    BbAvH: Average betting odds for home team win across bookmakers.
    BbMxD: Maximum betting odds for draw across bookmakers.
    BbAvD: Average betting odds for draw across bookmakers.
    BbMxA: Maximum betting odds for away team win across bookmakers.
    BbAvA: Average betting odds for away team win across bookmakers.
    BbOU: Number of bookmakers used to calculate over/under goals.
    BbMx>2.5: Maximum betting odds for over 2.5 goals across bookmakers.
    BbAv>2.5: Average betting odds for over 2.5 goals across bookmakers.
    BbMx<2.5: Maximum betting odds for under 2.5 goals across bookmakers.
    BbAv<2.5: Average betting odds for under 2.5 goals across bookmakers.
    BbAH: Number of bookmakers used to calculate Asian handicap.
    BbAHh: Asian handicap line.
    BbMxAHH: Maximum betting odds for Asian handicap home team win across bookmakers.
    BbAvAHH: Average betting odds for Asian handicap home team win across bookmakers.
    BbMxAHA: Maximum betting odds for Asian handicap away team win across bookmakers.
    BbAvAHA: Average betting odds for Asian handicap away team win across bookmakers.
    PSCH: Betting odds for home team win (from Pinnacle Sports closing odds).
    PSCD: Betting odds for draw (from Pinnacle Sports closing odds).
    PSCA: Betting odds for away team win (from Pinnacle Sports closing odds)
    """
    
#Ucitavanje podataka
utakmice = pandas.read_csv("season-1819_csv.csv")

#JEDINSTVENI TIMOVI U DATASETU
timovi = numpy.unique(utakmice[['HomeTeam', 'AwayTeam']].values)

crveni = []
zuti = []
brojUtakmica = []
prosek = []
pobede = []
porazi = []

#IZVLACENJE BROJ KARTONA, UTAKMICA, POBEDA I PORAZA ZA SVAKI TIM
for tim in timovi:
    crveniKartoni = 0
    zutiKartoni = 0
    brUtakmica = 0
    brPobeda = 0
    brPoraza = 0
    for index, row in utakmice.iterrows():
        if row['HomeTeam']==tim:
            crveniKartoni+=row['HR']
            zutiKartoni+=row['HY']
            brUtakmica+=1
            if row['FTR'] == 'H':
                brPobeda+=1
            else:
                brPoraza+=1
        if row['AwayTeam']==tim:
            crveniKartoni+=row['AR']
            zutiKartoni+=row['AY']
            brUtakmica+=1
            if row['FTR'] == 'A':
                brPobeda+=1
            else:
                brPoraza+=1
    crveni.append(crveniKartoni)
    zuti.append(zutiKartoni)
    brojUtakmica.append(brUtakmica)
    prosek.append((crveniKartoni+zutiKartoni)/brUtakmica)
    pobede.append(brPobeda)
    porazi.append(brPoraza)
    
crveni = numpy.array(crveni)
zuti = numpy.array(zuti)
brojUtakmica = numpy.array(brojUtakmica)
prosek = numpy.array(prosek)
pobede = numpy.array(pobede)
porazi = numpy.array(porazi)

#SASTAVLJANJE NOVE TABELE
tk = numpy.column_stack((timovi, crveni, zuti, brojUtakmica, prosek, pobede, porazi))
timovi_kartoni = pandas.DataFrame(tk, columns=['team', 'redTotal', 'yellowTotal', 'matchesPlayed', 'avgCard', 'wins', 'loses'])

#SORTIRANJE TABELE PO PROSEKU CRVENIH I ZUTIH KARTONA PO UTAKMICI
timovi_kartoni_sortirani = timovi_kartoni.sort_values(by='avgCard')

#IZDVAJANJE GRUBIH TIMOVA (ONI KOJI IMAJU NAJVECI PROSECAN BROJ KARTONA PO UTAKMICI)
grubi_timovi = timovi_kartoni_sortirani.tail(5)

print(grubi_timovi)


#GRAFIKON CRVENI I ZUTI KARTONI
fig, ax = plt.subplots()

team_array = grubi_timovi['team'].values
red_total_array = grubi_timovi['redTotal'].values
yellow_total_array = grubi_timovi['yellowTotal'].values

width = 0.35

ax.bar(team_array, red_total_array, width, label='Crveni kartoni', color='red')

x_positions = numpy.arange(len(team_array)) + width

ax.bar(x_positions, yellow_total_array, width, label='Zuti kartoni', color='yellow')

ax.set_xticks(x_positions)
ax.set_xticklabels(team_array, rotation=45)

ax.set_ylabel('Broj kartona')

ax.set_title('Broj crvenih i zutih kartona po timu')

ax.legend()

plt.tight_layout()
plt.show()


#GRAFIKON POBEDE I PORAZI
fig, ax = plt.subplots()

pobede_array = grubi_timovi['wins'].values
porazi_array = grubi_timovi['loses'].values

width = 0.35

ax.bar(team_array, pobede_array, width, label='Pobede')

x_positions = numpy.arange(len(team_array)) + width

ax.bar(x_positions, porazi_array, width, label='Porazi')

ax.set_xticks(x_positions)
ax.set_xticklabels(team_array, rotation=45)

ax.set_ylabel('Broj pobeda/poraza')

ax.set_title('Broj pobeda/poraza po timu')

ax.legend()

plt.tight_layout()
plt.show()


