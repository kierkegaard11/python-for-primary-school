l = [-7, -9, 18, 24]

print(l[0])

print(l[0]+3)

print(l[2])

print(l[-1])

#print(l[4])

print("----------Duzina liste-----------")

print(len(l))

print("----------Prvi zadatak-----------")
z = 0
for i in range (1,11):
    z = z+i

print(z)

print("----------Drugi zadatak-----------")
for broj in range (1,10,3):
    print(broj)

print("---------Treci zadatak-------------")
p = 1
for i in range(4):
    p=p+i
print(p)

print("---------Cetvrti zadatak-------------")

t = "Pera je procitao 2 knige"
r = t[8:16]
u = t[17:]
k = ' sam '
p = r+k+u
print(t[2])
print(p)

print("---------Peti zadatak-------------")
def izracunaj(a,b):
    #a=11, b=14
    a = a+4 #15
    b= b*3 #42...
    return a+b #57

print(izracunaj(3,4))
print(izracunaj(11,14))
print(izracunaj(22,17))
print(izracunaj(1,1))

print("---------Sesti zadatak-------------")
def PP(a,b):
    return a*b

print(PP(2,3))

print("---------Sedmi zadatak-------------")

def vratiNekuRec():
    rec = "Lenka"
    return rec

print(vratiNekuRec())

print("----------------VEZBANJE---------------")
niz = [12,3,6,17,22,14]
print(niz[3])
print(niz[0])
print(len(niz))
print(niz[len(niz)-2])
print(niz[3]+niz[4])
#print(niz[len(niz)])

print("----------------2222---------------")
niz = [12,3,6,17,22,14]
print(niz[2:5])
print(sum(niz))
print(sum(niz[2:5]))
print(min(niz[0:3]))
print(max(niz[0:3]))
print(min(niz[0], niz[5]))

print("----------------3---------------")
recenica = "Misi vozi skejt."
print(len(recenica)*2)
rec = recenica[1]+recenica[2]+recenica[14]+recenica[len(recenica)-10]+recenica[11]
print(rec)
drugaRec = recenica[5:9]
print(drugaRec)


print("----------------4444---------------")
y = 2
for broj in range (1,11,2):
    y = y+broj+2
    print(y)

print("----------------5555---------------")
y = -7
for i in range (8):
    y = y+i
print(y)

print("----------------6---------------")
lista = [2,5,4,12,45,53,11,3]
for i in range (2,len(lista)-1,2):
    print(lista[i])

print("----------------7---------------")
def izracunajPovrsinuKruga(precnik):
    poluprecnik = precnik/2
    povrsina = poluprecnik*poluprecnik*3.14
    return povrsina

precnikKruga1 = 2
precnikKruga2 = 4
povrsinaKruga1 = izracunajPovrsinuKruga(precnikKruga1)
povrsinaKruga2 = izracunajPovrsinuKruga(precnikKruga2)
print(povrsinaKruga1)
print(povrsinaKruga2)

print("----------------8---------------")
def izracunajObimPravougaonika(a, b):
    return a*b

duzina = 5
sirina = 12

print(izracunajObimPravougaonika(duzina,sirina))
