## Faza 1: Istrazivanje problema  
Dat je dataset sa rezultatima utakmica Bundeslige za sezonu 2018/2019.
Tabela ima sledece kolone:
Div,Date,HomeTeam,AwayTeam,FTHG,FTAG,FTR,HTHG,HTAG,HTR,HS,AS,HST,AST,HF,AF,HC,AC,HY,AY,HR,AR,B365H,B365D,B365A,BWH,BWD,BWA,IWH,IWD,IWA,PSH,PSD,PSA,WHH,WHD,WHA,VCH,VCD,VCA,Bb1X2,BbMxH,BbAvH,BbMxD,BbAvD,BbMxA,BbAvA,BbOU,BbMx>2.5,BbAv>2.5,BbMx<2.5,BbAv<2.5,BbAH,BbAHh,BbMxAHH,BbAvAHH,BbMxAHA,BbAvAHA,PSCH,PSCD,PSCA
Za vecinu kolona nije jasno sta predstavljaju, a na sajtu https://datahub.io/sports-data/german-bundesliga nije data dokumentacija dataseta, pa su znacenja kolona pronadjena sa teskocom.
U svakom slucaju, pronadjena su sledeca znacenja kolona:
Div: Division or league name (in this case, German Bundesliga).

Date: Date of the match.

HomeTeam: Name of the home team.

AwayTeam: Name of the away team.

FTHG: Full-time home team goals.

FTAG: Full-time away team goals.

FTR: Full-time result (H: Home team win, A: Away team win, D: Draw).

HTHG: Half-time home team goals.

HTAG: Half-time away team goals.

HTR: Half-time result (H: Home team leading, A: Away team leading, D: Draw).

HS: Home team shots.

AS: Away team shots.

HST: Home team shots on target.

AST: Away team shots on target.

HF: Home team fouls committed.

AF: Away team fouls committed.

HC: Home team corners.

AC: Away team corners.

HY: Home team yellow cards.

AY: Away team yellow cards.

HR: Home team red cards.

AR: Away team red cards.

B365H: Betting odds for home team win (from Bet365).

B365D: Betting odds for draw (from Bet365).

B365A: Betting odds for away team win (from Bet365).

BWH: Betting odds for home team win (from Betandwin).

BWD: Betting odds for draw (from Betandwin).

BWA: Betting odds for away team win (from Betandwin).

IWH: Betting odds for home team win (from Interwetten).

IWD: Betting odds for draw (from Interwetten).

IWA: Betting odds for away team win (from Interwetten).

PSH: Betting odds for home team win (from Pinnacle Sports).

PSD: Betting odds for draw (from Pinnacle Sports).

PSA: Betting odds for away team win (from Pinnacle Sports).

WHH: Betting odds for home team win (from William Hill).

WHD: Betting odds for draw (from William Hill).

WHA: Betting odds for away team win (from William Hill).

VCH: Betting odds for home team win (from VC Bet).

VCD: Betting odds for draw (from VC Bet).

VCA: Betting odds for away team win (from VC Bet).

Bb1X2: Number of bookmakers used to calculate match odds.

BbMxH: Maximum betting odds for home team win across bookmakers.

BbAvH: Average betting odds for home team win across bookmakers.

BbMxD: Maximum betting odds for draw across bookmakers.

BbAvD: Average betting odds for draw across bookmakers.

BbMxA: Maximum betting odds for away team win across bookmakers.

BbAvA: Average betting odds for away team win across bookmakers.

BbOU: Number of bookmakers used to calculate over/under goals.

BbMx>2.5: Maximum betting odds for over 2.5 goals across bookmakers.

BbAv>2.5: Average betting odds for over 2.5 goals across bookmakers.

BbMx<2.5: Maximum betting odds for under 2.5 goals across bookmakers.

BbAv<2.5: Average betting odds for under 2.5 goals across bookmakers.

BbAH: Number of bookmakers used to calculate Asian handicap.

BbAHh: Asian handicap line.

BbMxAHH: Maximum betting odds for Asian handicap home team win across bookmakers.

BbAvAHH: Average betting odds for Asian handicap home team win across bookmakers.

BbMxAHA: Maximum betting odds for Asian handicap away team win across bookmakers.

BbAvAHA: Average betting odds for Asian handicap away team win across bookmakers.

PSCH: Betting odds for home team win (from Pinnacle Sports closing odds).

PSCD: Betting odds for draw (from Pinnacle Sports closing odds).

PSCA: Betting odds for away team win (from Pinnacle Sports closing odds)

**Tabela ima 306 redova.**

## Faza 2: Analiza zadatka  

Potrebno je izdvojiti pet timova koji su poznati po gruboj igri (imaju najvise crvenih i zutih kartona po utakmici). Ovo je potrebno prikazati i dijagramom.

Zatim je potrebno prikazati i broj pobeda i poraza ovih timova.

Uocavamo da cemo za analizu koristiti kolone:
HomeTeam - domacin
AwayTeam - gost
HR - broj crvenih kartona domacina
AR - broj crvenih kartona gosta
HY - broj zutih kartona domacina
AY - broj zutih kartona gosta
FTR - pobednik utakmice ('H' - domacin, 'A' - gost)

Prvi deo zadatka se sastoji iz obrade podataka, ciji rezultat treba da bude broj crvenih i zutih kartona po svakom timu, kao i broj pobeda i poraza.
Zatim treba da izdvojimo najgrublje timove, a onda je potrebno ove podatke da prikazemo graficki.

koristicemo python biblioteke pandas, numpy i matplotlib.

## Faza 4: testiranje programskog koda i ispravljanje gresaka


Tokom rada na problemu, desavale su se razlicite sintaksne greske, koje su odmah otklonjene.

## Faza 6: Vrednovanje resenja

Nakon zavrsetka rada na projektu, uoceno je da su ispunjeni svi zahtevi zadatka.
