from time import gmtime, strftime
import time
import datetime
from datetime import timedelta

vreme = str(input('Unesite vreme u Beogradu u formatu HH.mm :'));
sati = int(vreme.split(".")[0]);
minuti = int(vreme.split(".")[1]);

datum = datetime.datetime(2021, 1, 1, sati, minuti, 0);

def izracunaj_vreme_u_tokiju(datum):
    vreme_u_tokiju = datum + timedelta(hours = 7);
    print("TOKIO:          " + str(format(vreme_u_tokiju, '%H:%M'))+ "  " + vrati_obicaj_u_tokiju(vreme_u_tokiju));

def izracunaj_vreme_u_londonu(datum):
    vreme_u_londonu = datum + timedelta(hours = -1);
    print("LONDON:         " + str(format(vreme_u_londonu, '%H:%M'))+ "  " + vrati_obicaj_u_londonu(vreme_u_londonu));

def izracunaj_vreme_u_mumbaju(datum):
    vreme_u_mumbaju = datum + timedelta(hours = 4);
    print("MUMBAJ:         " + str(format(vreme_u_mumbaju, '%H:%M'))+ "  " + vrati_obicaj_u_mumbaju(vreme_u_mumbaju));

def izracunaj_vreme_u_riu(datum):
    vreme_u_riu = datum + timedelta(hours = -5);
    print("RIO DE ZANEIRO: " + str(format(vreme_u_riu, '%H:%M'))+ "  " + vrati_obicaj_u_riu(vreme_u_riu));

def izracunaj_vreme_u_kairu(datum):
    vreme_u_kairu = datum;
    print("KAIRO:          " + str(format(vreme_u_kairu, '%H:%M'))+ "  " + vrati_obicaj_u_kairu(vreme_u_kairu));

def vrati_obicaj_u_tokiju(vreme):
    if(vreme.hour==7):
        return "Ljudi u Japanu se u ovo vreme spremaju i krecu na posao ili u skolu.";
    if(vreme.hour==8):
        return "Za dorucak, u Japanu se jedu jaja, pirinac i miso supa";
    if(vreme.hour==9 or vreme.hour == 10 or vreme.hour==11):
        return "U Japanu je tacnost deo kulturnog ponasanja. Kasnjenje je nepristojno.";
    if(vreme.hour==12):
        return "Zaposleni ljudi u Japanu rucaju oko 12 ili 13 casova.";
    if(vreme.hour==13):
        return "Ljudi koji rade i deca koja idu u skolu nose rucak spremljen kod kuce u kutiji koja se zove bento";
    if(vreme.hour==14 or vreme.hour==1 or vreme.hour==3):
        return "Sintoizam i Budizam su dve najzastupljenije religije u Japanu";
    if(vreme.hour==15 or vreme.hour==16):
        return "Japanski stripovi 'manga' i crtani filmovi 'anime' su veoma popularni sirom sveta.";
    if(vreme.hour==17 or vreme.hour==19 or vreme.hour==20):
        return "Jedan od najzanimljivijih delova Tokija je Harajuku koji je centar specificne japanske mode.";
    if(vreme.hour==18 or vreme.hour==21 or vreme.hour==22 or vreme.hour==23):
        return "Tokio se nalazi na japanskom ostrvu Honsu. Ostala ostrva su Hokaido, Sikoku i Kjusu.";
    return "Tokio je najveci grad na svetu. Nastao je spajanjem dva grada - Tokio i Jokohama i ima oko 38 miliona stanovnika."; 

def vrati_obicaj_u_londonu(vreme):
    if(vreme.hour==6 or vreme.hour==7):
        return "Popularni engleski dorucak se sastoji od jaja, pasulja, przene slanine i kobasica, pecuraka, paradajza i hleba";
    if(vreme.hour==8 or vreme.hour==9):
        return "Engleski dorucak se u Velikoj Britaniji ne jede tako cesto, vec uglavnom nedeljom i praznicima.";
    if(vreme.hour==10 or vreme.hour==11 or vreme.hour==13):
        return "Najpoznatiji muzeji u Londonu su muzej Tejt i muzej Viktorija i Albert i Prirodnjacki muzej.";
    if(vreme.hour==14 or vreme.hour==16):
        return "Kraljica Elizabeta je na tronu Ujedinjenog Kraljevstva od 1953. godine";
    if(vreme.hour==10 or vreme.hour==20 or vreme.hour==23):
        return "Kraljevska porodica zivi u Bakingemskoj palati koja ima 775 prostorija.";
    if(vreme.hour==12 or vreme.hour==0 or vreme.hour==2):
        return "Tower of London je jedna od najznamenitijih gradjevina u Londonu. Nekada je bila zatvor i mesto za pogubljenja, a danas je muzej.";
    if(vreme.hour==15 or vreme.hour==3 or vreme.hour==5):
        return "Ulica Oksford u Londonu je centar mode u Londonu i najprometnija ulica u Evropi sa 500000 posetilaca dnevno.";
    if(vreme.hour==17 or vreme.hour==18):
        return "Englezi tradicionalno ispijaju caj u 17 casova. Uz caj jedu kolace, tortu ili sendvice.";
    return "London je glavni grad Velike Britanije i u njemu zivi oko 9 miliona ljudi."; 

def vrati_obicaj_u_mumbaju(vreme):
    if(vreme.hour==6 or vreme.hour==11):
        return "Hinduizam je najzastupljenija religija u Indiji.";
    if(vreme.hour==7 or vreme.hour==8):
        return "Hindusi u Indiji dan zapocinju molitvom koje se naziva pudza (puja).";
    if(vreme.hour==9 or vreme.hour==10):
        return "Indijska tradicionalna jela su veoma zacinjena i sastoje se uglavnom od povrca i mlecnih proizvoda.";
    if(vreme.hour==22 or vreme.hour==23):
        return "Indijci u Mumbaju veceraju veoma kasno, oko 22 casa i to jela koja se spremaju na ulicnim tezgama pravo ispred vas.";
    if(vreme.hour==0 or vreme.hour==1):
        return "Plaza je najpopularnije mesto za vecernju setnju u Mumbaju.";
    if(vreme.hour==2 or vreme.hour==3 or vreme.hour==4 or vreme.hour==5):
        return "Elefantska pecina je znamenitost koju stiti UNESCO. U pecini se nalaze skulpture iz petog veka isklesane u stenama.";
    if(vreme.hour==12 or vreme.hour==13 or vreme.hour==14):
        return "Najpoznatiji bogovi i boginje u Hinduizmu su Brahma, Vishnu, Shiva, Ganapati...";
    if(vreme.hour==15 or vreme.hour==16 or vreme.hour==17):
        return "Reke u Indiji su Gang, Bramaputra, Godavari...";
    return "Mumbaj je grad u Indiji sa preko 20 miliona stanovnika."; 

def vrati_obicaj_u_riu(vreme):
    if(vreme.hour==12 or vreme.hour==1):
        return "Mladi u Rio de Zaneiru najvise vremena provode na plazi.";
    if(vreme.hour==5 or vreme.hour==6 or vreme.hour==7):
        return "Rio de Zaneiro znaci 'januarska reka'";
    if(vreme.hour==8 or vreme.hour==9 or vreme.hour==10):
        return "Dorucak u Brazilu se sastoji uglavnom od svezeg voca, kolaca, sendvica sa sunkom i sirom i kafe.";
    if(vreme.hour==11 or vreme.hour==12 or vreme.hour==13):
        return "Favele su delovi Rio de Zaneira u kojima zive siromasni. U favelama je stopa kriminala velika.";
    if(vreme.hour==14 or vreme.hour==15 or vreme.hour==16):
        return "Rio je poznat po ogromnoj statui Isusa Hrista koja se nalazi na brdu Korkovado, visokom 709 metara.";
    if(vreme.hour==17 or vreme.hour==18 or vreme.hour==19):
        return "Najpoznatije plaze u Rio de Zaneiru su Kopakabana, Ipanema i Leblon.";
    if(vreme.hour==20 or vreme.hour==21 or vreme.hour==22):
        return "Najveci karneval na svetu odrzava se u Rio de Zaneiru i u njemu ucestvuje oko 2 miliona ljudi svake godine.";
    if(vreme.hour==20 or vreme.hour==21 or vreme.hour==22):
        return "Karneval u Rio de Zaneiru je nastao 1723. godine i odrzava se krajem februara svake godine.";
    return "Rio de Zaneiro je nekada bio glavni grad Brazila i ima oko 12 miliona stanovnika.";

def vrati_obicaj_u_kairu(vreme):
    if(vreme.hour==1 or vreme.hour==2 or vreme.hour==3):
        return "Nacionalno jelo Egipta je koshari i sastoji se od pirinca i sociva sa przenim lukom i paradajzom.";
    if(vreme.hour==6 or vreme.hour==7 or vreme.hour==8):
        return "Koptski deo Kaira je deo gde zive hriscani. U njemu se nalazi poznata Viseca crkva.";
    if(vreme.hour==9 or vreme.hour==10 or vreme.hour==11):
        return "Piramide u Gizi su najveca znamenitost Kaira i celog Egipta.";
    if(vreme.hour==12 or vreme.hour==13 or vreme.hour==14):
        return "Ispred Keopsove, Kefrenove i Mikerinove piramide nalazi se statua Sfinge.";
    if(vreme.hour==15 or vreme.hour==16 or vreme.hour==17):
        return "U egipatskom gradu Luksoru nalazi se Dolina kraljeva u kojoj su sahranjivani faraoni i plemicke porodice.";
    if(vreme.hour==18 or vreme.hour==19 or vreme.hour==20):
        return "Najmladji faraon drevnog Egipta bio je Tutankamon i vladao je od svoje 9. do 19. godine.";
    if(vreme.hour==21 or vreme.hour==22 or vreme.hour==23):
        return "Najpoznatija kraljica drevnog Egipta bila je Nefertiti i sa svojim muzem je uvela verske reforme.";
    return "Kairo je glavni grad Egipta i nalazi se na delti Nila. Ima oko 8 miliona stanovnika."; 

print("BEOGRAD:        " + str(format(datum, '%H:%M')))
print("___________________________________________________________________________")
izracunaj_vreme_u_tokiju(datum);
print("___________________________________________________________________________")
izracunaj_vreme_u_londonu(datum);
print("___________________________________________________________________________")
izracunaj_vreme_u_mumbaju(datum);
print("___________________________________________________________________________")
izracunaj_vreme_u_riu(datum);
print("___________________________________________________________________________")
izracunaj_vreme_u_kairu(datum);
