import pygame
import math
#0bavezno
pygame.init()


(sirina, visina) = (300, 300)

#Obavezno - podesavanje prozora
prozor = pygame.display.set_mode((sirina, visina)) #duzina i sitina u px

#Naslov prozora
pygame.display.set_caption("Krugovi")

#Boja prozora
prozor.fill(pygame.Color(194, 90, 234))

#Crtanje kruga
#pygame.draw.circle(prozor, pygame.Color("red"), ((sirina/3),(visina/2)), 100, 2)

#Crtanje linije
# pygame.draw.line(prozor, pygame.Color(150, 41, 112), (50, 50), (150, 150), 2)

#Pravougaonik
# pygame.draw.rect(prozor, pygame.Color("orange"), (100, 100, 50, 50), 5)
# pygame.draw.rect(prozor, pygame.Color("blue"), (150, 100, 50, 50))
# pygame.draw.rect(prozor, pygame.Color("green"), (200, 100, 50, 50))
# pygame.draw.rect(prozor, pygame.Color("white"), (250, 100, 50, 50))

#Slovo G
# pygame.draw.line(prozor, pygame.Color("black"), (40, 40), (110, 40), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (40, 40), (40, 180), 5)

#Slovo A
# pygame.draw.line(prozor, pygame.Color("black"), (100, 50), (50, 150), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (100, 50), (150, 150), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (75, 100), (125, 100), 5)

#Znak plus
# pygame.draw.line(prozor, pygame.Color("black"), (100, 50), (100, 150), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (50, 100), (150, 100), 5)


#Slovo I
# pygame.draw.line(prozor, pygame.Color("black"), (50, 150), (150, 50), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (50, 50), (50, 150), 5)
# pygame.draw.line(prozor, pygame.Color("yellow"), (150, 50), (150, 150), 5)

#Slovo E
# pygame.draw.line(prozor, pygame.Color("black"), (50, 50), (150, 50), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (50, 50), (50, 180), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (50, 115), (125, 115), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (50, 180), (150, 180), 5)

#Slovo M
# pygame.draw.line(prozor, pygame.Color("black"), (50, 50), (50, 150), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (150, 50), (150, 150), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (50, 50), (100, 100), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (100, 100), (150, 50), 5)

#Slovo X
# pygame.draw.line(prozor, pygame.Color("black"), (50, 150), (150, 50), 5)
# pygame.draw.line(prozor, pygame.Color("black"), (50, 50), (150, 150), 5)

# pygame.draw.circle(prozor, pygame.Color("red"), (200,200), 10, 5)
# pygame.draw.circle(prozor, pygame.Color("red"), (200,200), 20, 5)
# pygame.draw.circle(prozor, pygame.Color("red"), (200,200), 30, 5)

# precnik = 10

# for i in range (10):
#     print(i)

# for i in range (2,10):
#     print(i)

# for i in range (2,10,3):
#     print(i)

#koncentricni krugovi
# for i in range (1,20):
#     pygame.draw.circle(prozor, pygame.Color("red"), (200,200), precnik, 5)
#     precnik = precnik+10

# pocetak = 0
# kraj = 100
# korak = 10
# for i in range (0, 100, 10):
#     pygame.draw.circle(prozor, pygame.Color("red"), (200,200), i, 5)

#merdevine
# leva strana
# pygame.draw.line(prozor, pygame.Color("brown"), (100, 10), (100, visina - 10), 10)
# # desna strana
# pygame.draw.line(prozor, pygame.Color("brown"), (200, 10), (200, visina - 10), 10)

# pygame.draw.line(prozor, pygame.Color("brown"), (100, 50), (200, 50), 10)

# for i in range(50, 260, 50):
#     pygame.draw.line(prozor, pygame.Color("brown"), (100, i), (200, i), 10)

#krugovi koji se dodiruju
#r=15

# for i in range(1,20,2):
#     pygame.draw.circle(prozor, pygame.Color("red"), (i*r, visina/2), r,5)


# pygame.draw.circle(prozor, pygame.Color("red"), (100, 100), 30,5)
# pygame.draw.circle(prozor, pygame.Color("red"), (145, 100), 30,5)
# pygame.draw.circle(prozor, pygame.Color("red"), (190, 100), 30,5)

# pygame.draw.circle(prozor, pygame.Color("red"), (127, 135), 30,5)
# pygame.draw.circle(prozor, pygame.Color("red"), (172, 135), 30,5)

#srce
pygame.draw.polygon(prozor, pygame.Color("red"), [(100,150),(150,100),(200,150),(150,200)])
pygame.draw.circle(prozor, pygame.Color("red"), (125, 125), 50*math.sqrt(2)/2)
pygame.draw.circle(prozor, pygame.Color("red"), (175, 125), 50*math.sqrt(2)/2)

# a=2
# for i in range(3, 22, 3):
#     print(i+a)

# for i in range(9,15):
#     print(i)

# for i in range(4,20,4):
#     print(i)

# a=2
# i=2
# for i in range(2,20,3):
#     print(a)
#     a=i+a

# #Obavezno - osvezavanje sadrzaja prozora
pygame.display.update()


#pygame.time.wait(5000) 
#pauza
while pygame.event.wait().type != pygame.QUIT:
    pass
pygame.quit()